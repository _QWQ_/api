<?php

namespace App\Http\Controllers;

use App\Models\AuthUser;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /* public function getLogin(Request $request)
     {
         $password = $request->route('password');

         \Auth::login();
         $users = \DB::select('select * from users where email = ? AND password = ?', array($request->route('email'), $request->route('pass')));
         dump(\Auth::check());
         $user = User::whereEmail($request->route('email'))->wherePassword($request->route('password'))->first();

         $user->remember_token = csrf_token();
         \Auth::login($user, true);

         dump(\Auth::attempt(array('email' => $request->route('email'), 'password' => $request->route('password'))));
         Auth::viaRemember();
         dump($user);
         dump(Auth::viaRemember());
         dump(\Auth::check());

         if($users) {
             return $users;
         }
     }*/


    /**
     * @param Request $request
     * @return \App\User|array|null
     */
    public function postLogin(Request $request)
    {
        $password = $request->input('password');
        $email = $request->input('email');
        return User::Login($email, $password);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function postForgotPassword(Request $request)
    {
        $email = $request->input('email');
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        return User::forgotPassword($email, $first_name, $last_name);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postRegister(Request $request)
    {
        $email = $request->input('email');
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $password = $request->input('password');
        $confirmPassword = $request->input('confirmpassword');
        return User::createUser($first_name, $last_name, $email, $password, $confirmPassword);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postResetPassword(Request $request)
    {
        $authToken = $request->input('token');
        $newPassword = $request->input('newpassword');
        $confirmPassword = $request->input('confirmpassword');
        return User::ResetPassword($newPassword, $confirmPassword, $authToken);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUserEdit(Request $request)
    {
        $email = $request->input('email');
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $oldpassword = $request->input('oldpassword');
        $newpassword = $request->input('newpassword');
        return User::UserEdit($email, $first_name, $last_name, $newpassword, $oldpassword);
    }
}
