<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Route::get('auth/login/{email}/{password}', 'Controller@getLogin');
//Route::post('auth/login', 'Auth\AuthController@test');

Route::post('auth/login', 'Controller@postLogin');
Route::post('auth/forgotpassword', 'Controller@postForgotPassword');
Route::post('auth/register', 'Controller@postRegister');

Route::post('user/resetpassword', 'Controller@postResetPassword')->middleware('token');
Route::post('user/edit', 'Controller@postUserEdit')->middleware('token');



