<?php

namespace App\Models;

use Auth;
use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Response;
use Validator;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return string
     */
    private static function generatePassword()
    {
        $newPassword = '';
        $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        $maxChar = 10;
        $size = StrLen($chars) - 1;
        while ($maxChar--) {
            $newPassword .= $chars[rand(0, $size)];
        }
        return $newPassword;
    }

    /**
     * @param $firstName
     * @param $lastName
     * @param $email
     * @param $password
     */
    public static function createUser($firstName, $lastName, $email, $password, $confirmPaswword)
    {
        if ($password !== $confirmPaswword) {
            return Response::json('Не верно введено подтверждение пароля!', 421);
        }
        $v = Validator::make([$email, $password], [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if ($v->fails()) {
            return Response::json(['errors' => $v->getMessageBag()->all()]);
        }

        $user = DB::select('select * from users where email = ?', [$email]);
        if (!$user) {
            User::create([
                'first_name' => $firstName,
                'email' => $email,
                'last_name' => $lastName,
                'password' => bcrypt($password),
            ]);
            /*\DB::table('users')->insert([
                'email' => $email,
                'first_name' => $firstName,
                'last_name' => $lastName,
                'password' => bcrypt($password),
                'created_at' => new DateTime()
            ]);*/
            return Response::json('Вы успешно зарегистрированы', 200);
        }
        return Response::json('Такой пользователь уже зарегистрирован', 421);

    }

    /**
     * @param $email
     * @param $first_name
     * @param $last_name
     * @param $newpassword
     * @param $oldpassword
     * @return \Illuminate\Http\JsonResponse
     */
    public static function UserEdit($email, $first_name, $last_name, $newpassword, $oldpassword)
    {
        $user = Auth::user();
        if (!empty($email)) {
            $user->email = $email;
        }
        if (!empty($first_name)) {
            $user->first_name = $first_name;
        }
        if (!empty($last_name)) {
            $user->last_name = $last_name;
        }
        if (($oldpassword != $newpassword) && ($oldpassword == $user->password)) {
            $user->password = bcrypt($newpassword);
        } else {
            return Response::json('Введите другой пароль', 421);
        }
        $user->save();
        return Response::json('Данные обновлены!', 200);
    }

    /**
     * @param $newPassword
     * @param $confirmPassword
     * @return \Illuminate\Http\JsonResponse
     */
    public static function ResetPassword($newPassword, $confirmPassword, $authToken)
    {
        if ($newPassword || $confirmPassword) {
            return Response::json('Пароль отствует', 421);
        }
        if ($newPassword === $confirmPassword) {
            $date = new DateTime();
            if (DB::update('UPDATE users SET password = ?, created_at = ? where auth_token = ? ', [$newPassword, $date, $authToken])) {
                return Response::json('Пароль успешно изменен', 200);
            }
        }
        return Response::json('Повторите ввод пароля', 421);
    }

    /**
     * @param $email
     * @param $first_name
     * @param $last_name
     * @return \Illuminate\Http\JsonResponse
     */
    public static function forgotPassword($email, $first_name, $last_name)
    {
        Auth::guard();

        if ($email && $first_name && $last_name) {
            $user = DB::select('select * from users where email = ? AND first_name = ? AND last_name = ?', [$email, $first_name, $last_name]);
            if (!$user) {
                return Response::json('Неверно укзаны данные!', 421);
            }
            $newPassword = static::generatePassword();

            DB::update('UPDATE users SET password = ? where email = ? AND first_name = ? AND last_name = ?', [bcrypt($newPassword), $email, $first_name, $last_name]);
            return \Response::json($newPassword, 200);
        }
    }

    public static function Login($email, $password)
    {
        $isLogged = Auth::attempt(['email' => $email, 'password' => $password]);

        if (!$isLogged) {
            return ['error' => 'Неверный логин или пароль'];
        }

        $user = Auth::user();
        $user->auth_token = bcrypt(time() . $user->email);
        $user->save();

        return Auth::user();
    }
}
